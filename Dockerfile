FROM maven:3.6.3-adoptopenjdk-15 AS package
COPY src /usr/src/application/src
COPY pom.xml /usr/src/application
RUN mvn -f /usr/src/application/pom.xml package

FROM adoptopenjdk:15-jre-hotspot as builder
WORKDIR application
COPY --from=package /usr/src/application/target/event-management-micronaut-0.1.jar application.jar
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-XX:+IdleTuningGcOnIdle", "-Xtune:virtualized", "-jar", "application.jar"]