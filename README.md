# Demo Java 15 + Micronaut 2.2.3

Useful resources:
https://micronaut-projects.github.io/micronaut-test/latest/guide/#junit5


docker login registry.gitlab.com
docker build -t registry.gitlab.com/ghena/event-management-micronaut .
docker push registry.gitlab.com/ghena/event-management-micronaut