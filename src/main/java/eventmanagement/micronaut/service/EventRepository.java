package eventmanagement.micronaut.service;

import eventmanagement.micronaut.controller.EventDto;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
public class EventRepository {

  public List<EventDto> getAllEvents() {
    List<EventDto> events = new ArrayList<>();
    events.add(new EventDto(1l, "''Aventuri cu dl. Goe'' | Ateneul National din Iasi"));
    events.add(new EventDto(2l, "Luna Francofoniei la Iasi"));
    events.add(new EventDto(3l, "Expoziţie de fotografie „Amintiri despre Cernobîl”"));
    return events;
  }

}
