package eventmanagement.micronaut.service;

import eventmanagement.micronaut.controller.EventDto;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class EventService {

  @Inject
  private EventRepository eventRepository;

  public List<EventDto> getAllEvents() {
    return eventRepository.getAllEvents();
  }

}
