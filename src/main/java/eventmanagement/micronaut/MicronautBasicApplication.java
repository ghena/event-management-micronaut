package eventmanagement.micronaut;

import io.micronaut.runtime.Micronaut;

public class MicronautBasicApplication {

  public static void main(String[] args) {
    Micronaut.run(MicronautBasicApplication.class, args);
  }
}
