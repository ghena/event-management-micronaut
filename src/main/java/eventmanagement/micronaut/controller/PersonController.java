package eventmanagement.micronaut.controller;

import eventmanagement.micronaut.service.EventService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.validation.Validated;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

@Validated
@Controller("/v1/events")
public class PersonController {

  @Inject
  private EventService eventService;

  //default it is already enabled to produce and consume JSON.

  @Post()
  public HttpResponse<?> saveEvent(@Body @Valid EventDto person) {
    return HttpResponse.status(HttpStatus.CREATED);
  }

  @Get()
  public List<EventDto> getAllEvents() {
    return eventService.getAllEvents();
  }
}
