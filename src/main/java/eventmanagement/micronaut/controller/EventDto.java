package eventmanagement.micronaut.controller;

import io.micronaut.core.annotation.Introspected;
import javax.validation.constraints.NotEmpty;

@Introspected
public class EventDto {

  private Long id;

  @NotEmpty
  private String title;

  public EventDto(Long id, @NotEmpty String title) {
    this.id = id;
    this.title = title;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
