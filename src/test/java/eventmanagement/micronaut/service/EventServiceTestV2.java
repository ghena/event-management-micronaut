package eventmanagement.micronaut.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import eventmanagement.micronaut.controller.EventDto;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import java.util.List;
import javax.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@MicronautTest
class EventServiceTestV2 {

  @Inject
  private EventService eventService;

  @Inject
  private EventRepository eventRepository;

  @MockBean(EventRepository.class)
  EventRepository eventRepository() {
    return mock(EventRepository.class);
  }

  @Test
  void shouldGetEvents() {
    // Given
    when(eventRepository.getAllEvents()).thenReturn(List.of(new EventDto(1l, "a")));

    // When
    List<EventDto> events = eventService.getAllEvents();

    // Then
    Assertions.assertEquals(1, events.size(), "There should be 1 events");
  }
}